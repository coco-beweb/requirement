                                    REQUIREMENTS (company: BeTruc)

Introduction:
    This specification defines the requirements and specifications for the implementation of an SSO system for 
    BeTruc, which has several applications requiring authentication. The aim is to provide a seamless login 
    experience for users, while guaranteeing data and account security.

1/ Project presentation
    1.1/ Project developers
        The company responsible for this project is Team2-5. It is based in Montpellier, at 59 avenue de Toulouse 
        in France, and is a web development agency.

    1.2/ Problem
        Betruc currently has several applications available for its customers. BeTruc customers must authenticate 
        to the various applications with their login credentials when they need to connect. These multiple login 
        requirements are a major inconvenience for users, and have caused BeTruc to lose customers due to the 
        repeated use of logins. 
        
        BeTruc would like to set up a centralized connection management system for all its users' applications.

    1.3/ Target
        To provide BeTruc with unique authentication for each of its applications, we decided to implement SSO.
        With SSO, a user only needs to enter their login details (username, password, etc.) once on a single page 
        to access all their applications.

2/ Functional description of requirements
    - Single sign-on (SSO):
        Users must be able to log in only once to access all compatible applications.
        Authentication must be secure, with support for two-factor validation if required.
    - Application integration:
        SSO must be compatible with a defined set of existing applications.
        Applications must be seamlessly integrated without requiring major changes to their source code.
    - Cross-platform compatibility:
        SSO must work on a variety of platforms, including desktops, mobile devices and web browsers.
    - Access Control and Authorization:
        SSO must integrate with existing authorization systems to ensure that users only have access resources
        for which they are authorized.
    - Security and confidentiality:
        SSO must follow security best practices, using robust encryption protocols and standards.

        BONUS:
    - Logging and Monitoring :
        The SSO system must provide activity logs and monitoring mechanisms to detect intrusion attempts or 
        abnormal behavior.


3/ Project scope
    This project does not support SSO-based user profile management, the password reset procedure or centralized 
    session management. arrangements can be made between BeTruc and Team2-5 to implement these features if the 
    necessary resources are deployed.
    Unit tests will not be included in this specification given the resources available at the time.

4/ Technical and logistical constraints
    4.1/ Budget
        The budget is 2000.00€.

    4.2/ Deadline
        The project will start on Monday, October 30, 2023 and must be delivered within 12 days.

    4.3/ Technical limitations
        Connection to applications should not be slowed down by SSO.

    4.4/ Technical specifications
        SSO functionality will be implemented via a RESTful API created in nodeJS. This API will 
        connect all BeTruc applications to each other. This involves a first 
        connection of the user to one of these applications. The SSO API will then generate a json web 
        token that will be sent to the user once authentication has been validated. This SSO token will then be 
        saved in the browser's local storage. This token will enable direct and immediate 
        authentication to any of the services provided by BeTruc.

        This API will be implemented in BeTruc applications via an SSO button. 
        "button (see figma).

5/ Deliverables
    5.1/ Complete documentation
        Technical documentation for SSO administration.
        Documentation for end users.
    5.2/ Source code:
        All source code developed for this project.
    5.3/ Operational SSO system:
        An operational SSO system integrated with existing applications.